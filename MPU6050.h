/*
 * MPU6050.h
 *
 *  Created on: 16 gru 2018
 *      Author: Skynets
 */

#ifndef MPU6050_H_
#define MPU6050_H_
/*
* FS_SEL | Full Scale Range   | LSB Sensitivity
* -------+--------------------+----------------
* 0      | +/- 250 degrees/s  | 131 LSB/deg/s	|.007633f(*)
* 1      | +/- 500 degrees/s  | 65.5 LSB/deg/s	|.015267f(*)
* 2      | +/- 1000 degrees/s | 32.8 LSB/deg/s	|.030487f(*)
* 3      | +/- 2000 degrees/s | 16.4 LSB/deg/s	|.060975f(*)
*
* AFS_SEL | Full Scale Range | LSB Sensitivity
* --------+------------------+----------------
* 0       | +/- 2g           | 16384 LSB/mg		|.000061f(*)
* 1       | +/- 4g           | 8192 LSB/mg		|.000122f(*)
* 2       | +/- 8g           | 4096 LSB/mg		|.000122f(*)
* 3       | +/- 16g          | 2048 LSB/mg		|.000122f(*)
*/
#define MPU6050_FS_SEL 3
#define MPU6050_AFS_SEL 0

#define MPU6050_GYRO_LSB .060975f
#define MPU6050_ACCEL_LSB .000061f

#define MPU6050_ADDR_R 0xD1
#define MPU6050_ADDR_W 0xD0

#define MPU6050_GYRO_CONFIG 0x1B
#define MPU6050_ACCEL_CONFIG 0x1C
#define MPU6050_ACCEL_XOUT_H 0x3B
#define MPU6050_ACCEL_XOUT_L 0x3C
#define MPU6050_ACCEL_YOUT_H 0x3D
#define MPU6050_ACCEL_YOUT_L 0x3E
#define MPU6050_ACCEL_ZOUT_H 0x3F
#define MPU6050_ACCEL_ZOUT_L 0x40
#define MPU6050_TEMP_OUT_H 0x41
#define MPU6050_TEMP_OUT_L 0x42
#define MPU6050_GYRO_XOUT_H 0x43
#define MPU6050_GYRO_XOUT_L 0x44
#define MPU6050_GYRO_YOUT_H 0x45
#define MPU6050_GYRO_YOUT_L 0x46
#define MPU6050_GYRO_ZOUT_H 0x47
#define MPU6050_GYRO_ZOUT_L 0x48

#define MPU6050_PWR_MGMT_1 0x6B
#define MPU6050_PWR_MGMT_2 0x6C
#define MPU6050_WHO_AM_I 0x70

float MPU6050_Temperature;

typedef struct
{
	int16_t x;
	int16_t y;
	int16_t z;
}Vector;

Vector Accel;
Vector AccelNormalize;
Vector Gyro;
Vector GyroCalibrate;
Vector GyroNormalize;
Vector Angle;


void MPU6050_Initalize(void);
void MPU6050_Active(void);
void MPU6050_WakeUp(void);
void MPU6050_getTemperature(void);
void MPU6050_GyroConfig(void);
void MPU6050_getGyro(void);
void MPU6050_NormalizeGyro(void);
void MPU6050_AccelConfig(void);
void MPU6050_getAccel(void);
void MPU6050_NormalizeAccel(void);
void MPUU6050_Angle(void);
uint16_t MPU6050_getScoreTemperature(void);
void MPU6050_CalibrateGyro(uint8_t samples);

#endif /* MPU6050_H_ */
