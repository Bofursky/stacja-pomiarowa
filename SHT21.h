/*
 * SHT21.h
 *
 *  Created on: 14 gru 2018
 *      Author: Skynets
 */


#ifndef SHT21_H_
#define SHT21_H_

#define SHT21_ADDRW 0x80 //Adres czujnika
#define SHT21_ADDRR 0x81 //Adres czujnika
#define SHT21_REG  0xE7
#define SHT21_TEMP 0xE3
#define SHT21_HUM 0xE5

uint16_t Temp;
uint16_t Hum;

void SHT21_Initalize(void);
void SHT21_getTemperature(void);
void SHT21_getHumidity(void);
void SHT21_Register(void);
uint16_t SHT21_getScoreTemperature(void);
uint16_t SHT21_getScoreHumidity(void);

#endif /* SHT21_H_ */
