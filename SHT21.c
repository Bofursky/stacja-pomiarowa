/*
 * SHT21.c
 *
 *  Created on: 14 gru 2018
 *      Author: Skynets
 */
#include <util/delay.h>
#include "I2C.h"
#include "SHT21.h"
#include "usart.h"

void SHT21_Initalize(void)
{
	I2C_Start();
	I2C_SendAddr(SHT21_ADDRW);
	I2C_Stop();

}
void SHT21_getTemperature(void)
{
	I2C_Start();
	I2C_SendAddr(SHT21_ADDRW);
	I2C_SendByte(SHT21_TEMP);
	_delay_ms(85);
	I2C_Start();
	I2C_SendAddr(SHT21_ADDRR);
	Temp = ( I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK() );
	I2C_Stop();
}
void SHT21_getHumidity(void)
{
	I2C_Start();
	I2C_SendAddr(SHT21_ADDRW);
	I2C_SendByte(SHT21_HUM);
	_delay_ms(29);
	I2C_Start();
	I2C_SendAddr(SHT21_ADDRR);
	Hum = ( I2C_ReceiveData_ACK() << 8) | ( I2C_ReceiveData_NACK() );
	I2C_Stop();
}
void SHT21_Register(void)
{
	I2C_Start();
	I2C_SendAddr(SHT21_ADDRW);
	I2C_SendByte(SHT21_REG);
	I2C_Start();
	I2C_SendAddr(SHT21_ADDRR);
	USART_SendValue(I2C_ReceiveData_NACK(), 2);
	I2C_Stop();

}
uint16_t SHT21_getScoreTemperature(void)
{
	SHT21_getTemperature();
	float Temp1;
	uint16_t temperature;
	Temp &= ~0x0003;

	Temp1 = (-46.85 + 175.72/65536 * Temp)*100;
	temperature = Temp1;
	return temperature;
}
uint16_t SHT21_getScoreHumidity(void)
{
	SHT21_getHumidity();
	float Hum1;
	uint16_t humidity;
	Hum &= ~0x0003;

	Hum1 = (-6.0 + 125.0/65536 * Hum)*100;
	humidity = Hum1;
	return humidity;
}


