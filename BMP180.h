
#ifndef BMP180_H_
#define BMP180_H_

#define BMP180_ADDR 0xEE        // Adres I2C
#define BMP180_CHIP_ID 0xD0		// Id Chip
#define BMP180_MODE 3           // oversampling setting (0-3)
#define BMP180_Temp 0x2E

typedef struct
{
	int		AC1;
	int		AC2;
	int		AC3;
	unsigned int	AC4;
	unsigned int	AC5;
	unsigned int	AC6;
	int		B1;
	int		B2;
	int		MB;
	int		MC;
	int		MD;
}BMP180_INIT;

extern BMP180_INIT bmp180_init;

void BMP180_Initalize(void);
void BMP180_Active(void);
void BMP180_getTemperature(void);
void BMP180_getPressure(void);
long BMP180_getScoreTemperature(void);
long BMP180_getScorePressure(void);

#endif /* BMP180_H_ */
