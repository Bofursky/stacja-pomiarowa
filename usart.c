#include <avr/io.h>
#include <stdlib.h>

#include "usart.h"

void USART_Initalize(void)
{
	UBRR0L = UBRR;
	UBRR0H = (UBRR >> 8);
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	UCSR0C = (3<<UCSZ00);
}

void USART_SendChar(char data)
{
	while ( !( UCSR0A & (1<<UDRE0)) );
	UDR0 = data;
}

void USART_SendText(char *s)
{
	while(*s) USART_SendChar(*s++);
}
void USART_SendValue(int value, int radix)
{
	char string[17];
	itoa(value, string, radix);
	USART_SendText(string);
}
