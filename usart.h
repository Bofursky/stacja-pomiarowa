#ifndef USART_H_
#define USART_H_

#define USART_BAUDRATE 19200
#define UBRR ((F_CPU+USART_BAUDRATE*8UL) / (16UL*USART_BAUDRATE)-1)

void USART_Initalize(void);
void USART_SendChar(char znak);
void USART_SendText(char *s);
void USART_SendValue(int value, int radix);

#endif
