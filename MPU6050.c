/*
 * MPU6050.c
 *
 *  Created on: 16 gru 2018
 *      Author: Skynets
 */
#include <util/delay.h>

#include "I2C.h"
#include "MPU6050.h"

void MPU6050_Initalize(void)
{
	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_W);
	I2C_Stop();

}
void MPU6050_WakeUp(void)
{
	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_W);
	I2C_SendByte(MPU6050_PWR_MGMT_1);
	I2C_SendByte(0x00);
	I2C_Stop();
}
void MPU6050_AccelConfig(void)
{
	uint8_t value;

	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_W);
	I2C_SendByte(MPU6050_ACCEL_CONFIG);
	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_R);
	value = I2C_ReceiveData_NACK();
	I2C_Stop();

	value &= 0b11100111;
	value |= (MPU6050_AFS_SEL << 3);

	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_W);
	I2C_SendByte(MPU6050_ACCEL_CONFIG);
	I2C_SendByte(value);
	I2C_Stop();
}
void MPU6050_getAccel(void)
{

	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_W);
	I2C_SendByte(MPU6050_ACCEL_XOUT_H);
	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_R);
	Accel.x = (I2C_ReceiveData_ACK() <<8 | I2C_ReceiveData_ACK());
	Accel.y = (I2C_ReceiveData_ACK() <<8 | I2C_ReceiveData_ACK());
	Accel.z = (I2C_ReceiveData_ACK() <<8 | I2C_ReceiveData_NACK());
	I2C_Stop();
}
void MPU6050_NormalizeAccel(void)
{
	MPU6050_getAccel();
	AccelNormalize.x = Accel.x * MPU6050_ACCEL_LSB * 9.80665f;
	AccelNormalize.y = Accel.y * MPU6050_ACCEL_LSB * 9.80665f;
	AccelNormalize.z = Accel.z * MPU6050_ACCEL_LSB * 9.80665f;

}
void MPUU6050_Angle(void)
{
	MPU6050_NormalizeAccel();
	const float pi = 3.141592;
	Angle.x = atan2(AccelNormalize.x, sqrt(square(AccelNormalize.y) + square(AccelNormalize.z)))/(pi/180);
	Angle.y = atan2(AccelNormalize.y, sqrt(square(AccelNormalize.x) + square(AccelNormalize.z)))/(pi/180);
}

void MPU6050_GyroConfig(void)
{
	uint8_t value;

	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_W);
	I2C_SendByte(MPU6050_GYRO_CONFIG);
	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_R);
	value = I2C_ReceiveData_NACK();
	I2C_Stop();

	value &= 0b11100111;
	value |= (MPU6050_FS_SEL << 3);

	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_W);
	I2C_SendByte(MPU6050_GYRO_CONFIG);
	I2C_SendByte(value);
	I2C_Stop();

}
void MPU6050_getGyro(void)
{
	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_W);
	I2C_SendByte(MPU6050_GYRO_XOUT_H);
	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_R);
	Gyro.x = (I2C_ReceiveData_ACK() <<8 | I2C_ReceiveData_ACK());
	Gyro.y = (I2C_ReceiveData_ACK() <<8 | I2C_ReceiveData_ACK());
	Gyro.z = (I2C_ReceiveData_ACK() <<8 | I2C_ReceiveData_NACK());
	I2C_Stop();
}
void MPU6050_CalibrateGyro(uint8_t samples)
{
	int16_t sumX = 0;
	int16_t sumY = 0;
	int16_t sumZ = 0;

    // Read n-samples
    for (uint8_t i = 0; i < samples; ++i)
    {
    	//Srednia krocz�ca tutaj wstawic
    	MPU6050_getGyro();
		sumX += Gyro.x;
		sumY += Gyro.y;
		sumZ += Gyro.z;

	_delay_ms(5);
    }

    GyroCalibrate.x = sumX / samples;
    GyroCalibrate.y = sumY / samples;
    GyroCalibrate.z = sumZ / samples;

}
void MPU6050_NormalizeGyro(void)
{
	MPU6050_getGyro();

	GyroNormalize.x = (Gyro.x - GyroCalibrate.x) * MPU6050_GYRO_LSB;
	GyroNormalize.y = (Gyro.y - GyroCalibrate.y) * MPU6050_GYRO_LSB;
	GyroNormalize.z = (Gyro.z - GyroCalibrate.z) * MPU6050_GYRO_LSB;
}
void MPU6050_getTemperature(void)
{
	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_W);
	I2C_SendByte(MPU6050_TEMP_OUT_H);
	I2C_Start();
	I2C_SendAddr(MPU6050_ADDR_R);
	MPU6050_Temperature = (I2C_ReceiveData_ACK() <<8 | I2C_ReceiveData_NACK());
	I2C_Stop();
}
uint16_t MPU6050_getScoreTemperature(void)
{
	MPU6050_getTemperature();
	float T;
	T = (MPU6050_Temperature/340+35.53)*100;
	return T;
}
