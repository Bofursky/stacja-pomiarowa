#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdlib.h>

#include "usart.h"
#include "I2C.h"
#include "BH1750.h"
#include "SHT21.h"
#include "BMP180.h"
#include "MPU6050.h"

//250000
int main(void)
{
	_delay_ms(500);

	USART_Initalize();
	sei();// globalne odblokowanie przerwa�
	USART_SendText("Inicjalizacja UART");
	USART_SendChar('\r');
	USART_SendChar('\n');

	I2C_Initalize();
	USART_SendText("Inicjalizacja I2C     ");
	USART_SendChar('\r');
	USART_SendChar('\n');

	BH1750_Initalize();
	USART_SendText("Inicjalizacja BH1750: ");
	if(I2C_Error == 0 )
	{
		USART_SendText("Good");
	}
	else
	{
		USART_SendText("Bad");
	}
	USART_SendChar('\r');
	USART_SendChar('\n');

	SHT21_Initalize();
	USART_SendText("Inicjalizacja SHT21: ");
	if(I2C_Error == 0 )
	{
		USART_SendText("Good");
	}
	else
	{
		USART_SendText("Bad");
	}
	USART_SendChar('\r');
	USART_SendChar('\n');

	BMP180_Initalize();
	USART_SendText("Inicjalizacja BMP180: ");
	if(I2C_Error == 0 )
	{
		USART_SendText("Good");
		BMP180_Active();
	}
	else
	{
		USART_SendText("Bad");
	}
	USART_SendChar('\r');
	USART_SendChar('\n');

	MPU6050_Initalize();
	USART_SendText("Inicjalizacja MPU6050: ");
	if(I2C_Error == 0 )
	{
		USART_SendText("Good");
		MPU6050_WakeUp();
		MPU6050_GyroConfig();
		MPU6050_AccelConfig();
		MPU6050_CalibrateGyro(50);
	}
	else
	{
		USART_SendText("Bad");
	}
	USART_SendChar('\r');
	USART_SendChar('\n');

	DDRB |= (1<<PB0);
	PORTB &= ~(1<<PB0);

	//uint8_t L = 0b10111101;
	//uint8_t H = 0b10000011;
	//uint16_t c;
	//H ^=(3 << 3);

	//USART_SendValue(H, 2);
	//USART_SendChar('\r');
	//USART_SendChar('\n');
	//H ^=(0x00 << 2);
	//USART_SendValue(H, 2);
	while(1)
    {
		//MPUU6050_Angle();
		//sprintf()
	//	MPU6050_getAccel();
		//MPU6050_NormalizeAccel();
		//USART_SendValue(AccelNormalize.x, 10);
		//USART_SendText("   ");
		//USART_SendValue(AccelNormalize.y, 10);
		//USART_SendText("   ");
	//	USART_SendValue(AccelNormalize.z, 10);
		//USART_SendText("           ");
		//USART_SendValue(Accel.x, 10);
	//	USART_SendText("   ");
		//USART_SendValue(Accel.y, 10);
		//USART_SendText("   ");
		//USART_SendValue(Accel.z, 10);
	//	USART_SendChar('\r');
		//USART_SendChar('\n');
//
		PORTB ^=(1<<PB0);
		_delay_ms(1000);
		//MPU6050_NormalizeGyro();
		//USART_SendText(">>MPU: ");
		//USART_SendValue(MPU6050_getScoreTemperature(), 10);
		//USART_SendText("   ");
		//USART_SendValue(MPU6050_getScoreTemperature(), 2);
		//USART_SendText("<<");
		//USART_SendChar('\r');
		//USART_SendText(">>");
		USART_SendValue(BH1750_getScoreLux(), 10);
		//USART_SendText("<<");
		USART_SendChar('\r');
		//USART_SendText(">>");

	//USART_SendChar('\n');
	//SHT21_Register();
		//I2C_OK            0
		//I2C_STARTError	1
		//I2C_NoNACK		3
		//I2C_NoACK		    4
		//int temp = BH1750_getScoreLux();
		//int temp = SHT21_getScoreTemp();

	}
}


