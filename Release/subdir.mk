################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../BH1750.c \
../BMP180.c \
../I2C.c \
../MPU6050.c \
../SHT21.c \
../main.c \
../usart.c 

OBJS += \
./BH1750.o \
./BMP180.o \
./I2C.o \
./MPU6050.o \
./SHT21.o \
./main.o \
./usart.o 

C_DEPS += \
./BH1750.d \
./BMP180.d \
./I2C.d \
./MPU6050.d \
./SHT21.d \
./main.d \
./usart.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -ffunction-sections -fdata-sections -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


